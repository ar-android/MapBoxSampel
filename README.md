# MapBox Demo

Penggunaan layanan pemetaan pihak ketiga, yang bernama MapBox. Penggunaanya tanpa menggunakan Google Play Services seperti Google Maps, tapi menggunakan Open Street Map dan Mapzen.

Dokumentasi tentang MapBox dapat dilihat di :

  - [MapBox Website]
  - [MapBox Developer Web]
  - [Mapzen]
  - [Mapzen Github]
  - [Mapzen LOST]

[MapBox Website]:<https://www.mapbox.com/>
[MapBox Developer Web]:<https://www.mapbox.com/developers/>
[Mapzen]:<https://mapzen.com/>
[Mapzen Github]:<https://github.com/mapzen>
[Mapzen LOST]:<https://github.com/mapzen/LOST>