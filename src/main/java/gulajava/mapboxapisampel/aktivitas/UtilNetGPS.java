package gulajava.mapboxapisampel.aktivitas;

import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.mapbox.mapboxsdk.geometry.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gulajava Ministudio on 2/6/16.
 */
public class UtilNetGPS {

    private boolean isInternet = false;
    private boolean isNetGPS = false;
    private NetworkInfo netinfo = null;

    private LocationManager lokasimanager = null;
    private ConnectivityManager conmanager = null;


    private Context mContext;

    public UtilNetGPS(Context context) {
        mContext = context;

        isInternet = false;
        isNetGPS = false;

        lokasimanager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        conmanager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

    }


    public boolean cekStatusInternet() {

        netinfo = conmanager.getActiveNetworkInfo();
        isInternet = netinfo != null && netinfo.isConnected();

        return isInternet;
    }

    public boolean cekStatusNetworkGPS() {

        try {
            isNetGPS = lokasimanager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
            isNetGPS = false;
        }

        return isNetGPS;
    }


    public List<LatLng> getListLokasi() {

        List<LatLng> latLngList = new ArrayList<>();
        latLngList.add(new LatLng(-6.891981, 107.610439));
        latLngList.add(new LatLng(-6.897987, 107.613640));
        latLngList.add(new LatLng(-6.901727, 107.615861));
        latLngList.add(new LatLng(-6.905933, 107.618657));
        latLngList.add(new LatLng(-6.906645, 107.615979));
        latLngList.add(new LatLng(-6.900928, 107.612531));
        latLngList.add(new LatLng(-6.885039, 107.613603));
        latLngList.add(new LatLng(-6.888809, 107.618457));
        latLngList.add(new LatLng(-6.900860, 107.621435));

        return latLngList;
    }


}
